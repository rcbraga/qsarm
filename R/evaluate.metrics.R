evaluate <- function(stantard=1, ... ){
library(car)
#####################################################
Yobs.training <- model.train$obs
Ypred.training <- model.train$pred
Ncompounds.training <- nrow(model.train)
Yobs.test  <- model.test$obs
Ypred.test <- model.test$pred
Ncompounds.test <- nrow(model.test)
#####################################################
cat("##### slopes of the regression lines k #####\n")
K<-qsarm::K(Yobs.test,Ypred.test)
print(K)
cat("##### slopes of the regression lines kline #####\n")
Kline<-qsarm::Kline(Yobs.test,Ypred.test)
print(Kline)
cat("##### Rsquared #####\n")
print(postResample(model.train$pred, model.train$obs))
R2<-R2(model.train$pred, model.train$obs, formula = "corr", na.rm = FALSE)
print(R2)
R2(model.train$pred, model.train$obs, formula = "corr", na.rm = FALSE)
cat("##### CCC (concordance correlation coefficient) for Training Set#####\n")
print(qsarm::CCC(Ypred.training, Yobs.training, Ncompounds.training))
CCCtrain=qsarm::CCC(Ypred.training, Yobs.training, Ncompounds.training)
cat("##### R2 for Test Set\n")
R2ext<-R2(model.test$pred, model.test$obs, formula = "corr", na.rm = FALSE)
print(R2ext)
cat("##### Q2f1 for Test or External Validation - Old R2Pred #####\n")
print(qsarm::r2pred(Ypred.test, Yobs.test, Yobs.training))
Q2f1<-qsarm::r2pred(Ypred.test, Yobs.test, Yobs.training)
cat("##### Q2f2 for Test or External Validation #####\n")
print(qsarm::Q2f2(Ypred.test, Yobs.test, Yobs.training))
Q2f2=qsarm::Q2f2(Ypred.test, Yobs.test, Yobs.training)
cat("##### Q2f3 for Test or External Validation - Most Accepeted #####\n")
print(qsarm::Q2f3(Ypred.test, Yobs.test,Yobs.training, Ncompounds.test, Ncompounds.training))
Q2f3=qsarm::Q2f3(Ypred.test, Yobs.test,Yobs.training, Ncompounds.test, Ncompounds.training)
cat("##### CCC (concordance correlation coefficient) for Test or External Validation#####\n")
print(qsarm::CCC(Ypred.test, Yobs.test, Ncompounds.test))
CCC=qsarm::CCC(Ypred.test, Yobs.test, Ncompounds.test)
cat("##### RMSEC - Root Mean Square (RMS) for Training Set#####\n")
print(qsarm::RMSEC(Yobs.training,Ypred.training, Ncompounds.training))
RMSEC=qsarm::RMSEC(Yobs.training,Ypred.training, Ncompounds.training)
cat("##### RMSEP - Root Mean Square (RMS) for Test Set#####\n")
print(qsarm::RMSEP(Yobs.test,Ypred.test, Ncompounds.test))
RMSEP=qsarm::RMSEP(Yobs.test,Ypred.test, Ncompounds.test)
cat("##### MAE - Mean Absolute Error for Test Set#####\n")
print(qsarm::MAE(Yobs.test,Ypred.test, Ncompounds.test))
MAE=qsarm::MAE(Yobs.test,Ypred.test, Ncompounds.test)
cat("#####Rm2 for Test or External Validation#####\n")
rm2=qsarm::rm2(Yobs.test,Ypred.test)
cat("#####Rm2 Reverse for Test or External Validation#####\n")
rm2.reverse=qsarm::rm2.reverse(Yobs.test,Ypred.test)
cat("##### AVERAGE Rm2 for Test or External Validation#####\n")
average.rm2=qsarm::average.rm2(Yobs.test,Ypred.test)
cat("#####DELTA Rm2 for Test or External Validation#####\n")
delta.rm2=qsarm::delta.rm2(Yobs.test,Ypred.test)
cat("#####Durbin-Watson Trainig for the Training Set#####\n")
print(durbinWatsonTest(as.vector(((Yobs.training))  - ((Ypred.training)))))
DWtrain=durbinWatsonTest(as.vector(((Yobs.training))  - ((Ypred.training))))
cat("#####Durbin-Watson Test for the Test Set#####\n")
print(durbinWatsonTest(as.vector(((Yobs.test))  - ((Ypred.test)))))
DWtest=durbinWatsonTest(as.vector(((Yobs.test))  - ((Ypred.test))))
cat("#####Durbin-Watson Test for the Overall Set#####\n")
print(durbinWatsonTest(as.vector((rbind(Yobs.training,Yobs.test))  - (rbind(Ypred.training,Ypred.test)))))
DW=durbinWatsonTest(as.vector((rbind(Yobs.training,Yobs.test))  - (rbind(Ypred.training,Ypred.test))))
#table
dwbias=500
q2value=501
CCCvalue=502
average.rm2.value=503
delta.rm2.value=504
M <- list(Value=matrix(c(Q2f1,Q2f1>0.6,q2value,Q2f2,Q2f2>0.6,q2value,Q2f3,Q2f3>0.6,q2value,CCC,CCC>0.8,CCCvalue,RMSEC,RMSEC,RMSEC,RMSEP,RMSEP,RMSEP,MAE,MAE,MAE,average.rm2,average.rm2>0.5,average.rm2.value,delta.rm2,delta.rm2<0.2,delta.rm2.value,DWtest,DWtest,dwbias,DWtrain, DWtrain,dwbias,DW,DW,dwbias),byrow=TRUE,ncol=3))
rownames(M$Value) <- c("Q2ext(f1)","Q2ext(f2)","Q2ext(f3)","CCC", "RMSEC", "RMSEP", "MAE","Average rm2", "Delta rm2", "DurbinWatson.Traininig", "DurbinWatson.Test", "DurbinWatson.overall")
colnames(M$Value) <- c("Result", "Assessment", "Criterion")
M=as.data.frame(M$Value)
M$Assessment[M$Assessment==1.00] <- "PASS"
M$Assessment[M$Assessment==0.00] <- "FAIL" 
M[12,2][M[12,2]<1.95] <- "Negative autocorrelation"
M[11,2][M[11,2]<1.95] <- "Negative autocorrelation"
M[10,2][M[10,2]<1.95] <- "Negative autocorrelation"
M[12,2][M[12,2]>=2.1] <- "Positive autocorrelation"
M[11,2][M[11,2]>=2.1] <- "Positive autocorrelation"
M[10,2][M[10,2]>=2.1] <- "Positive autocorrelation"
M$Criterion[M$Criterion==500]<- "<2 and >2 biased; =2 unbiased"
M$Criterion[M$Criterion==501]<- "> 0.6"
M$Criterion[M$Criterion==502]<- "> 0.8"
M$Criterion[M$Criterion==503]<- "> 0.5"
M$Criterion[M$Criterion==504]<- "< 0.2"

.GlobalEnv[["M"]] <- M

}


evaluate.csv <- function(stantard=1,decimals=2, ... ){
library(car)
library(plyr)
library(ggplot2)
  #####################################################
  Yobs.training <- Trainind[,1]
  Ypred.training <- Trainind[,2]
  Ncompounds.training <- nrow(as.data.frame(Yobs.training))
  Yobs.test  <- Test[,1]
  Ypred.test <- Test[,2]
  Ncompounds.test <- nrow(as.data.frame(Yobs.test))
  #############G & T parameters
  r.gt <- cor(Ypred.test,Yobs.test) # 1)
  r2ext.gt <- qsarm::r2pred(Ypred.test, Yobs.test, Yobs.training) # 2)
  r20.gt <- R2(Ypred.test, Yobs.test, formula = "corr", na.rm = FALSE)
  r2oorigin.gt <- r2(Yobs.test,(lm(Yobs.test ~ -1 + Ypred.test)))
  K<-qsarm::K(Yobs.test,Ypred.test)
  Kline<-qsarm::Kline(Yobs.test,Ypred.test)
  gt3= (r.gt- r20.gt)/ r.gt
  gt4= (r.gt- r2oorigin.gt)/ r.gt
  gt5 = abs(r20.gt - r2oorigin.gt)
  ####################################
  R2<-R2(Ypred.training, Yobs.training, formula = "corr", na.rm = FALSE)
  R2(Ypred.training, Yobs.training, formula = "corr", na.rm = FALSE)
  CCCtrain=qsarm::CCC(Ypred.training, Yobs.training, Ncompounds.training)
  R2ext<-R2(Ypred.test, Yobs.test , formula = "corr", na.rm = FALSE)
  Q2f1<-qsarm::r2pred(Ypred.test, Yobs.test, Yobs.training)
  Q2f2=qsarm::Q2f2(Ypred.test, Yobs.test, Yobs.training)
  Q2f3=qsarm::Q2f3(Ypred.test, Yobs.test,Yobs.training, Ncompounds.test, Ncompounds.training)
  CCC=qsarm::CCC(Ypred.test, Yobs.test, Ncompounds.test)
  RMSEC=qsarm::RMSEC(Yobs.training,Ypred.training, Ncompounds.training)
  RMSEP=qsarm::RMSEP(Yobs.test,Ypred.test, Ncompounds.test)
  RMSEP.overall=qsarm::RMSEP((append(Yobs.training,Yobs.test)), (append(Ypred.training,Ypred.test)), (Ncompounds.training+Ncompounds.test) )
  MAE=qsarm::MAE(Yobs.test,Ypred.test, Ncompounds.test)
  rm2=qsarm::rm2(Yobs.test,Ypred.test)
  rm2.reverse=qsarm::rm2.reverse(Yobs.test,Ypred.test)
  average.rm2=qsarm::average.rm2(Yobs.test,Ypred.test)
  delta.rm2=qsarm::delta.rm2(Yobs.test,Ypred.test)
  DWtrain=durbinWatsonTest(as.vector(((Yobs.training))  - ((Ypred.training))))
  DWtest=durbinWatsonTest(as.vector(((Yobs.test))  - ((Ypred.test))))
  DW=durbinWatsonTest(as.vector((append(Yobs.training,Yobs.test))  - (append(Ypred.training,Ypred.test))))
#Histogram and Density Plots
hist.training <- data.frame(Dataset = factor( rep(c("Training"), each=Ncompounds.training) ), 
                            Residuals = c(Yobs.training-Ypred.training))
hist.test<- data.frame(Dataset = factor( rep(c("Test"), each=Ncompounds.test) ), 
                       Residuals = c(Yobs.test-Ypred.test))
hist.total=rbind(hist.training,hist.test)
cdf<- ddply(hist.total, "Dataset", summarise, rating.mean=mean(Residuals))
#Histograms
plot1=ggplot(hist.total, aes(x=Residuals, fill=Dataset)) +
  geom_histogram(binwidth=.02, alpha=.5, position="identity") +
  geom_vline(data=cdf, aes(xintercept=rating.mean,  colour=Dataset),
linetype="dashed", size=1)
plot2=ggplot(hist.total, aes(x=Residuals, colour=Dataset)) + geom_density() +
  geom_vline(data=cdf, aes(xintercept=rating.mean,  colour=Dataset),
             linetype="dashed", size=1)
multiplot(plot1, plot2, cols=2)
.GlobalEnv[["plot1"]] <- plot1
ggsave(filename="Overlaid.histograms.with.means.png", plot=plot1, width=14.20, height=8.57,dpi=200, units = 'cm')
.GlobalEnv[["plot1"]] <- plot1
.GlobalEnv[["plot2"]] <- plot2
ggsave(filename="Density.plots.with.means.png", plot=plot2,width=14.20, height=8.57,dpi=200, units = 'cm')


cat("#############################  qsarm 1.4 Statistical Results  ###########################\n")
cat("\n")
#table
  dwbias=500
  q2value=501
  CCCvalue=502
  average.rm2.value=503
  delta.rm2.value=504
  RMSEC.c=505
  No.value=506
  RMSEP.overall.c=507
  GT.c=508
  GT=509
  M <- list(Value=matrix(c(Q2f1,Q2f1>0.7,q2value,Q2f2,Q2f2>0.7,q2value,Q2f3,Q2f3>0.7,q2value,CCC,CCC>0.85,CCCvalue,RMSEC,No.value,RMSEC.c,RMSEP,No.value,RMSEC.c,RMSEP.overall,No.value,RMSEP.overall.c,MAE,No.value,RMSEC.c,average.rm2,average.rm2>0.65,average.rm2.value,delta.rm2,delta.rm2<0.2,delta.rm2.value,DWtest,DWtest,dwbias,DWtrain, DWtrain,dwbias,DW,DW,dwbias,GT,(r.gt > 0.6 &  r2ext.gt > 0.5 & gt3 <0.1 &  gt4 < 0.1 & gt5 <0.3 & 0.85<=K & K <= 1.15 & 0.85<=Kline & Kline <= 1.15),GT.c),byrow=TRUE,ncol=3))
  rownames(M$Value) <- c("Q2ext(f1)","Q2ext(f2)","Q2ext(f3)","CCC", "RMSEC", "RMSEP","RMSEP.overall", "MAE","Average rm2", "Delta rm2", "DurbinWatson.Trainig", "DurbinWatson.Test", "DurbinWatson.overall", "G & T")
  colnames(M$Value) <- c("Result", "Assessment", "Criterion")
  M=as.data.frame(M$Value)
  M$Assessment[M$Assessment==1.00] <- "PASS"
  M$Assessment[M$Assessment==0.00] <- "FAIL" 
  M$Assessment[M$Assessment==506] <- "-" 
  M$Criterion[M$Criterion==500]<- "<2 and >2 biased; =2 unbiased"
  M$Criterion[M$Criterion==501]<- "> 0.5 acceptable; >= 0.7 High Predictivy"
  M$Criterion[M$Criterion==502]<- "> 0.85"
  M$Criterion[M$Criterion==503]<- "> 0.65"
  M$Criterion[M$Criterion==504]<- "< 0.2"
  M$Criterion[M$Criterion==505]<- "Lower values indicate better fit"
  M$Criterion[M$Criterion==507]<- "RMSE=MAE = magnitude; >difference >varaince"
  M$Criterion[M$Criterion==508] <- "Golbraikh and Tropsha evaluation method"
  M$Result = round(M$Result,decimals)
  .GlobalEnv[["M"]] <- M
  get.autocorrelation(M[13,2])
  M[13,2] <- valueString
  get.autocorrelation(M[12,2])
  M[12,2] <- valueString
  get.autocorrelation(M[11,2])
  M[11,2] <- valueString
  M$Result[M$Result==509] <- "-"
  .GlobalEnv[["M"]] <- M
  print(M)
cat("\n")
cat("Please cite: Melo-Filho,CC ; Braga,RC ; Andrade,CH  Mol. Inf. 2014 XXXX\n")
cat("Bugs and Suggestions - Rodolpho at labmol.group@gmail.com  \n")
}


get.autocorrelation <- function( dwValue = 2, ... ){
  
  {if (dwValue >= 2.1)  { 
    
    dwValue <- "(+) autocorrelation"
    .GlobalEnv[["valueString"]] <- dwValue
    
  } else if (dwValue < 1.9) {
    
    dwValue <- "(-) autocorrelation"
    .GlobalEnv[["valueString"]] <- dwValue
    
  } else if (dwValue > 1.91 & dwValue < 1.95) {
    
    dwValue <- "(-)/no autocorrelation"
    .GlobalEnv[["valueString"]] <- dwValue
    
  } else if (dwValue > 2.05 & dwValue < 2.1) {
    
    dwValue <- "(+)/no autocorrelation"
    .GlobalEnv[["valueString"]] <- dwValue
    
  } else { 
    dwValue <- "No autocorrelation"
    .GlobalEnv[["valueString"]] <- dwValue
  }

}}



